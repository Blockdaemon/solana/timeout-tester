const express = require('express');
const app = express();

// path with number of seconds to sleep
app.get('/sleep/:seconds', (req, res) => {
  const sleepTime = parseInt(req.params.seconds) * 1000;
  // Sleep
  setTimeout(() => {
    res.send(`Slept for ${req.params.seconds} seconds`);
  }, sleepTime);
});

app.listen(3000, () => {
  console.log('Listening on port 3000');
});
