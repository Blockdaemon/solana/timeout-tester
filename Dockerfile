FROM node:18@sha256:b39895225fb1984139d5af76400aff8fac3dd5bc00dd41a3ce22fc8a6cf538d5

# Create app directory
WORKDIR /usr/src/app


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

EXPOSE 3000

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .



CMD [ "node", "index.js" ]

